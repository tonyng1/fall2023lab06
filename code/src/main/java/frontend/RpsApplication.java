package frontend;

import backend.RpsGame;
import javafx.application.*;
import javafx.stage.*;
import javafx.scene.*;
import javafx.scene.paint.*;
import javafx.scene.control.*;
import javafx.scene.layout.*;

/**
 * GUI application for rock paper scissors game
 *
 */
public class RpsApplication extends Application {
    public void start(Stage stage) {

        //Group object
        Group root = new Group(); 

        //RpsGame object
        RpsGame game = new RpsGame();

        //Container elements
        HBox buttons = new HBox();
        buttons.setPrefHeight(50);
        HBox textfields = new HBox();
        textfields.setPrefHeight(50);
        VBox overall =  new VBox();

        //Buttons
        Button rock = new Button("rock");
        Button paper = new Button("paper");
        Button scissors = new Button("scissors");

        //Text
        TextField message = new TextField("Welcome!");
        message.setPrefWidth(500);
        message.setEditable(false);
        TextField wins = new TextField("Wins: ");
        wins.setPrefWidth(100);
        wins.setEditable(false);
        TextField losses = new TextField("Losses: ");
        losses.setPrefWidth(100);
        losses.setEditable(false);
        TextField ties = new TextField("Ties: ");
        ties.setPrefWidth(100);
        ties.setEditable(false);

        //adding buttons/text to container elements
        buttons.getChildren().addAll(rock, paper, scissors);
        textfields.getChildren().addAll(wins, losses, ties);
        overall.getChildren().addAll(buttons, textfields, message);

        //adding overall to Group object, root
        root.getChildren().add(overall);

        //Adding events to the buttons
        rock.setOnAction(new RpsChoice("rock", wins, losses, ties, message, game));
        paper.setOnAction(new RpsChoice("paper", wins, losses, ties, message, game));
        scissors.setOnAction(new RpsChoice("scissors", wins, losses, ties, message, game));

        
        //create scene and associate to root
        Scene scene = new Scene(root, 850, 300); 
        scene.setFill(Color.LIGHTBLUE);
        overall.setTranslateX(scene.getWidth()/4);
        overall.setTranslateY(scene.getHeight()/4);

        //associate scene to stage and show
        stage.setTitle("Rock, Paper, Scissors game"); 
        stage.setScene(scene); 
        stage.show(); 
    }
    public static void main(String[] args) {
        Application.launch(args);
    }
}
