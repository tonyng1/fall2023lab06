package frontend;

import backend.RpsGame;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.*;

public class RpsChoice implements EventHandler<ActionEvent> {

    //fields
    private String playerChoice;
    private TextField wins;
    private TextField losses;
    private TextField ties;
    private TextField message;
    private RpsGame game;

    public RpsChoice(String playerChoice, TextField wins, TextField losses, TextField ties, TextField message, RpsGame game){
        this.playerChoice = playerChoice;
        this.wins = wins;
        this.losses = losses;
        this.ties = ties;
        this.message = message;
        this.game = game;
    }

    @Override
    public void handle(ActionEvent e){
        //TODO

        message.setText(game.playRound(playerChoice));
        wins.setText("Wins: " +game.getWins());
        losses.setText("Losses: "+game.getLosses());
        ties.setText("Ties: "+game.getTies());
    }
    
}
