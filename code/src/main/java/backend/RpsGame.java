package backend;
import java.util.Random;


public class RpsGame {
    private int wins;
    private int ties;
    private int losses;
    private Random r;

    public RpsGame(){
        this.wins = 0;
        this.ties = 0;
        this.losses = 0;
       this. r = new Random();
    }

    public String playRound(String choice){
        int num = r.nextInt(3);
        //0 = rock
        //1 = scissoors
        //2 = paper
        switch (choice) {
            case "rock":
            switch (num) {
                case 0:
                this.ties++;
                return "Computer played rock and it's a tie";

                case 1:
                wins++;
                return ("Computer played scissors and you won");

                case 2:
                losses++;
                return ("Computer played paper and the computer won");

            }
            break;
            case "scissors":
            switch (num) {
                case 0: 
                losses++;
                return ("Computer played rock and the computer won");

                case 1:
                ties++;
                return("Computer played scissors and it's a tie");
               

                case 2:
                wins++;
                return ("Computer played paper and you won");
            }
            break;

            case "paper":
            switch (num){
                case 0:
                wins++;
                return("Computer played rock and you won");
                

                case 1:
                losses++;
                return ("Computer played scissors the computer won");

                case 2:
                ties++;
                return ("Computer played paper it's a tie");
            }
            break;
        }
        return "";


    }
    
    public int getWins(){
        return this.wins;
    }

    public int getTies(){
        return this.ties;
    }

    public int getLosses(){
        return this.losses;
    }
}
