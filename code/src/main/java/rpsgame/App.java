package rpsgame;
import backend.RpsGame;
import java.util.Scanner;

/**
 * Console application for rock paper scissors game
 *
 */
public class App 
{
    public static void main( String[] args )
    {   
        Scanner reader = new Scanner(System.in);
        RpsGame rpsgame = new RpsGame();
        boolean playAgain = true;
        while(playAgain) {
            System.out.println("Please choose rock, paper or scissors");
            String userChoice = reader.nextLine();
            System.out.println(rpsgame.playRound(userChoice));
            System.out.println("Wins: " + rpsgame.getWins() + ", Losses: " + rpsgame.getLosses() + ", Ties: " + rpsgame.getTies());
            System.out.println("Would you like to play again?, type yes or no" );
            String choice = reader.nextLine();
            if(choice.equals("no")) {
                playAgain = false;
                System.out.println("Thanks for playing! Goodbye!");
            } else if (choice.equals("yes")) {
                System.out.println("Running another round...");
            } else {
                System.out.println("Invalid input!");
            }
        }
        
    }
}
